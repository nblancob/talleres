﻿# Talleres POO
## 1.Tangram
### Propósito
Repasar los conceptos fundamentales de la programación estructurada en la implementación del juego del tangram.
### Objetivos
1. Dibujo de las piezas
2. Selección y manipulación de la piezas
3. Verificación de resultados
4. Creación de niveles (opcional)
### 2.Tangram POO
### Propósito
Evidenciar los conceptos fundamentales de la programación orientada a objetos(POO).