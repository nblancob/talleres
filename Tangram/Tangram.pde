int level=1, l=0;
//Matrix Figuras
int [][] fig={{150, 75}, 
  {75, 150}, 
  {265, 75}, 
  {265, 225}, 
  {150, 188}, 
  {225, 150}, 
  {113, 263}};
//Color textos
color c=color(81, 209, 246);
//Declaracion variable mirror
float m;
//Declaración variable de angulo
float dx;
//Arreglo de rotacion de las figuras
float [] rot={0, 0, 0, 0, 0, 0, 0};
//Declaración de seleccion
boolean p;
//Image ganaste
PImage win; 

void setup() {
  size(1000, 600, P3D);
  win=loadImage("win.jpg");
}

//Guarda la variable de la rueda del mouse para rotar la pieza
void mouseWheel(MouseEvent event) {
  float e = event.getCount();
  dx=e;
  redraw();
}
//Seleccion la pieza y modifica la variable de rotacion
void seleccion() {
  if (dist(fig[0][0], fig[0][1], mouseX, mouseY)<60||dist(fig[1][0], fig[1][1], mouseX, mouseY)<60||dist(fig[2][0], fig[2][1], mouseX, mouseY)<60||dist(fig[3][0], fig[3][1], mouseX, mouseY)<60||dist(fig[4][0], fig[4][1], mouseX, mouseY)<50||dist(fig[5][0], fig[5][1], mouseX, mouseY)<50||dist(fig[6][0], fig[6][1], mouseX, mouseY)<50) {
    cursor(HAND);
    if (dist(fig[0][0], fig[0][1], mouseX, mouseY)<60&&mousePressed&&mouseButton==LEFT) {
      fig[0][0]=mouseX;
      fig[0][1]=mouseY;
      if (dx==1) {
        rot[0]=rot[0]+radians(15);
      } else if (dx==-1) {
        rot[0]=rot[0]-radians(15);
      }
    } else if (dist(fig[1][0], fig[1][1], mouseX, mouseY)<60&&mousePressed&&mouseButton==LEFT) {
      fig[1][0]=mouseX;
      fig[1][1]=mouseY;
      if (dx==1) {
        rot[1]=rot[1]+radians(15);
      } else if (dx==-1) {
        rot[1]=rot[1]-radians(15);
      }
    } else if (dist(fig[2][0], fig[2][1], mouseX, mouseY)<60&&mousePressed&&mouseButton==LEFT) {
      fig[2][0]=mouseX;
      fig[2][1]=mouseY;
      if (dx==1) {
        rot[2]=rot[2]+radians(15);
      } else if (dx==-1) {
        rot[2]=rot[2]-radians(15);
      }
    } else if (dist(fig[3][0], fig[3][1], mouseX, mouseY)<60&&mousePressed&&mouseButton==LEFT) {
      fig[3][0]=mouseX;
      fig[3][1]=mouseY;
      if (dx==1) {
        rot[3]=rot[3]+radians(15);
      } else if (dx==-1) {
        rot[3]=rot[3]-radians(15);
      }
    } else if (dist(fig[4][0], fig[4][1], mouseX, mouseY)<45&&mousePressed&&mouseButton==LEFT) {
      fig[4][0]=mouseX;
      fig[4][1]=mouseY;
      if (dx==1) {
        rot[4]=rot[4]+radians(15);
      } else if (dx==-1) {
        rot[4]=rot[4]-radians(15);
      }
    } else if (mousePressed&&dist(fig[5][0], fig[5][1], mouseX, mouseY)<50&&mouseButton==LEFT) {
      fig[5][0]=mouseX;
      fig[5][1]=mouseY;
      if (dx==1) {
        rot[5]=rot[5]+radians(15);
      } else if (dx==-1) {
        rot[5]=rot[5]-radians(15);
      }
    } else if (mousePressed&&dist(fig[6][0], fig[6][1], mouseX, mouseY)<50 && mouseButton==LEFT) {
      fig[6][0]=mouseX;
      fig[6][1]=mouseY;
      if (dx==1) {
        rot[6]=rot[6]+radians(15);
      } else if (dx==-1) {
        rot[6]=rot[6]-radians(15);
      }
    }
  } else {
    cursor(ARROW);
  }
}
//Funcion que refleja el paralelogramo
void mirror() {
  if (dist(fig[6][0], fig[6][1], mouseX, mouseY)<50&& mousePressed && mouseButton==RIGHT ) {
    m=m+PI;
    delay(50);
  }
}
void retornar() {
  if (mousePressed && mouseButton==CENTER) {
    fig[0][0]=150;
    fig[0][1]=75;
    fig[1][0]=75;
    fig[1][1]=150;
    fig[2][0]=265;
    fig[2][1]=75;
    fig[3][0]=265;
    fig[3][1]=225;
    fig[4][0]=150;
    fig[4][1]=188;
    fig[5][0]=225;
    fig[5][1]=150;
    fig[6][0]=113;
    fig[6][1]=263;
    for (int i=0; i<7; i++) {
      rot[i]=0;
    }
    m=0;
  }
}
//Pinta las figuras
void pintar() {
  textSize(20);
  push();
  translate(fig[0][0], fig[0][1]);
  rotate(rot[0]);
  fill(#008F39);
  triangle(-150, -75, 150, -75, 0, 75);
  fill(c);
  text("1", 0, 0);
  pop();
  fill(#FF8000);
  push();
  translate(fig[1][0], fig[1][1]);
  rotate(rot[1]);
  triangle(-75, -150, -75, 150, 75, 0);
  fill(c);
  text("2", 0, 0);
  pop();
  fill(#804000);
  push();
  translate(fig[2][0], fig[2][1]);
  rotate(rot[2]);
  triangle(35, -75, 35, 75, -40, 0);
  fill(c);
  text("3", 0, 0);
  pop();
  fill(#FF0000);
  push();
  translate(fig[3][0], fig[3][1]);
  rotate(rot[3]);
  triangle(35, -75, 35, 75, -115, 75);
  fill(c);
  text("4", 0, 20);
  pop();
  fill(#0000FF);
  push();
  translate(fig[4][0], fig[4][1]);
  rotate(rot[4]);
  triangle(0, -38, -75, 37, 75, 37);
  fill(c);
  text("5", 0, 0);
  pop();
  fill(#FFFF00);
  push();
  translate(fig[5][0], fig[5][1]);
  rotate(rot[5]);
  quad(-75, 0, 0, -75, 75, 0, 0, 75);
  fill(c);
  text("6", 0, 0);
  pop();
  fill(#8C004B);
  push();
  translate(fig[6][0], fig[6][1]);
  rotate(rot[6]);
  rotateX(m);
  quad(-113, 37, 37, 37, 112, -38, -38, -38);
  fill(c);
  text("7", 0, 0);
  pop();
}
//Crea la figura o nivel en este caso solo hay programados dos niveles
void figura() {
  fill(255, 255, 255);
  noStroke();
  switch (level) {
  case 1:
    triangle(588.2705, 541.1299, 888.2705, 541.1299, 738.2705, 391.1299);
    triangle(844.3365, 497.1959, 844.3365, 391.1299, 950.4025, 391.1299);
    triangle(632.2045, 497.1959, 526.1385, 391.1299, 738.2705, 391.1299);
    quad(844.3365, 285.0639, 950.4025, 285.0639, 950.4025, 391.1299, 844.3365, 391.1299);
    triangle(844.3365, 497.1959, 844.3365, 285.0639, 632.2045, 285.0639);
    triangle(632.2045, 391.1299, 526.1385, 285.0639, 526.1385, 391.1299);
    quad(738.2705, 391.1299, 632.2045, 285.0639, 526.1385, 285.0639, 632.2045, 391.1299);
    break;
  case 2:
    triangle(923.1155, 331.4848, 773.1155, 481.4848, 623.1155, 331.4848);
    triangle(623.1155, 331.4848, 835.2475, 543.6168, 623.1155, 543.6168);
    triangle(623.1155, 543.6168, 517.0495, 437.5508, 623.1155, 331.4848);
    triangle(517.0495, 437.5508, 517.0495, 287.5508, 592.0495, 362.5508);
    quad(592.0495, 212.5508, 667.0495, 287.5508, 592.0495, 362.5508, 517.0495, 287.5508);
    quad(592.0495, 212.5508, 667.0495, 287.5508, 667.0495, 137.5508, 592.0495, 62.5508);
    triangle(592.0495, 62.5508, 592.0495, 168.6168, 485.9835, 168.6168);
    break;
  }
}
// Comprueba si se relleno la figura, analizado el color del pixel
void comprobar() {
  color c;
  int d=0;
  for (int j=57; j<547; j++) {
    for (int i=479; i<953; i++) {
      c=get(i, j);
      if (c==-1) {
        d=d-1;
      } else if (c==-16777216) {
        d=d+0;
      } else {
        d=d+1;
      }
    }
  }
  if (d>86000&&d<93000) {
    clear();
    image(win, 200, 0);
    delay(300);
    if (mouseButton==RIGHT) {
      level=2;
    }
  }
  d=0;
}
void draw() {
  background(0, 0, 0);
  mirror();
  figura();
  seleccion();
  retornar();
  pintar();
  if (mousePressed==false) {
    comprobar();
  }
  dx=0;
}
