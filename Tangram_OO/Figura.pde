//Superclass Figuras
abstract class  Figura {
  PVector coor=new PVector();
  float ang=0, mir=0;
  color col;
  int i;

  Figura (float tempX, float tempY, int ti,color tcol) {
    coor.x=tempX;
    coor.y=tempY;
    i=ti;
    col=tcol;
  }

  void seleccion() {
    if (dist(coor.x, coor.y, mouseX, mouseY)<90) {
      cursor(HAND);
    } else {
      cursor(ARROW);
    }
  }

  void movimientos() {
    seleccion();
    if (mousePressed  && dist(coor.x, coor.y, mouseX, mouseY)<70) {
      if (mouseButton==LEFT) {
        coor.x=mouseX;
        coor.y=mouseY;
        if (dx==1) {
          ang=ang+radians(15);
        } else if (dx==-1) {
          ang=ang-radians(15);
        }
      } else if (mouseButton==RIGHT) {
        mir=mir+PI;
      }
    }
  }
  void draw() {
    textSize(20);
    noStroke();
    push();
    translate (coor.x, coor.y);
    rotate(ang);
    fill(col);
    drawFigure();
    fill(81, 209, 246);
    text(i, 0, -5);
    pop();
  }
  abstract void drawFigure();
}
