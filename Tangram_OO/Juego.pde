class Juego {
  PImage level;
  boolean dif=true,w=false;
  void cargar(int i) {
    level=loadImage("level"+i+".jpg");
  }
  void mostrar() {
    image(level, 0, 0);
    if(w){
    av.play();
    image(win, 200, 0);
    }  
}
  void comprobar() {
    color c;
    int d=0;
    for (int j=0; j<height; j++) {
      for (int i=301; i<width; i++) {
        c=get(i, j);
        if (c==-16777216) {
          d=d-1;
        } else if (c==-1) {
          d=d+0;
        } else {
          d=d+0;
        }
      }
    }
    println(d);
    if (d>-1050&&d<0) {
      println("ganaste");
      w=true;
    }
  }
  void modo() {
    if (dif==true) {
      int m=minute();
      int s=second();
      String t=nf(m, 2)+":"+nf(s, 2);
      fill(229, 43, 80);
      textSize(30);
      text(t, 0, height-60);
    }
  }
}
