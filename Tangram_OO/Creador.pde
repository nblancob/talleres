class Creador {
  PImage home;
  PImage save;
  int j=1;

  Creador() {
    home=loadImage("home.jpg");
    save=loadImage("save.jpg");
  }

  void button() {
    image(home, 0, height-45);
    image(save, 50, height-40);
    fill(0, 0, 0);
    if (dist(mouseX, mouseY, 20, height-25)<17.5) {
      cursor(HAND);
      if (mousePressed) {
        l=0;
      }
    } else if (dist(mouseX, mouseY, 68, height-22)<17.5) {
      cursor(HAND);
      if (mousePressed) {
        l=2;
      }
    } else {
      cursor(ARROW);
    }
  }
  void salvar(int j) {
    if (l==2) {
      j=j+1;
      loadPixels();
      for (int i=0; i<(width*height); i++) {
        if (pixels[i]!=color(255,255,255)) {
          pixels[i]=black;
        } else {
        }
        l=5;
      }
      updatePixels();
      save("data/level"+j+".jpg");
    }
  }
}
