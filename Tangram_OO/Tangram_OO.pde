import ddf.minim.*;
Minim start;
Minim ave;
AudioPlayer av;
AudioPlayer player;
float dx=0;
float mov;
int l=0, i=2;
int x;
boolean drawGrid;
PImage win;
PImage menu;
color black=color(0, 0, 0);
Figura [] figuras;
Triangulo triangulo1;
Cuadrilatero paralelogramo;
Creador nivel;
Juego [] game;
void setup() {
  size(1000, 600, P3D);
  start=new Minim(this);
  ave=new Minim(this);
  av=ave.loadFile("ave.mp3");
  player=start.loadFile("inicio.mp3");
  win=loadImage("win.jpg");
  game= new Juego[i];
  for (int j=0; j<i; j++) {
    game [j]=new Juego();  
    game [j].cargar(j);
  }
  nivel= new Creador();  
  figuras=new Figura[7];
  figuras [0] = new Triangulo(150, 75, 1, color(#008F39), -150, -75, 150, -75, 0, 75);
  figuras [1] = new Triangulo(75, 150, 2, color(#FF8000), -75, -150, -75, 150, 75, 0);
  figuras [2] = new Triangulo(265, 75, 3, color(#804000), 35, -75, 35, 75, -40, 0);
  figuras [3] = new Triangulo(265, 225, 4, color(#FF0000), 35, -75, 35, 75, -115, 75);
  figuras [4] = new Triangulo(150, 188, 5, color(#0000FF), 0, -38, -75, 37, 75, 37);
  figuras [5] =  new Cuadrilatero(225, 150, 6, color(#FFFF00), -75, 0, 0, -75, 75, 0, 0, 75);
  figuras [6] = new Cuadrilatero(113, 263, 7, color(#8C004B), -113, 37, 37, 37, 112, -38, -38, -38);
}
void draw() {
  if (key=='1') {
    x=1;
  } else if (key=='0') {
    x=0;
  }
  println(x);
  if (l==0) {
    menu();
  }
  if (l==4) {
    if (mousePressed==false) {
      game[x].comprobar();
    }
  }
  if (l!=0) {
    background(255, 255, 255);
  }
  if (l==4) {
    game[x].mostrar();
    game[0].modo();
  }
  if (mov==-32768) {
    figuras[1].movimientos();
  } else if (mov==-16740551) {
    figuras[0].movimientos();
  } else if (mov==-8372224) {
    figuras[2].movimientos();
  } else if (mov==-65536) {
    figuras[3].movimientos();
  } else if (mov==-16776961) {
    figuras[4].movimientos();
  } else if (mov==-256) {
    figuras[5].movimientos();
  } else if (mov==-7602101) {
    figuras[6].movimientos();
  }
  if (l!=0) {
    for (Figura figura : figuras) {
      figura.draw();
    }
  }
  if (drawGrid) {
    drawGrid(10);
  }
  if (l!=0) {
    nivel.button();
  }
  if (l!=0 && l!=4) {  
    nivel.salvar(0);
  }
  dx=0;
  mov=get(mouseX, mouseY);
}

void mouseWheel(MouseEvent event) {
  dx=event.getCount();
  redraw();
}
void drawGrid(float scale) {
  push();
  fill(255, 255, 255);
  strokeWeight(1);
  int i;
  for (i=0; i<=width/scale; i++) {
    stroke(0, 0, 0, 20);
    line(i*scale, 0, i*scale, height);
  }
  for (i=0; i<=height/scale; i++) {
    stroke(0, 0, 0, 20);
    line(0, i*scale, width, i*scale);
  }
  pop();
}
void keyPressed() {
  if (key == 'g' || key == 'G')
    drawGrid = !drawGrid;
}
void menu() {
  menu=loadImage("menu.jpg");
  image(menu, 0, 0);
  fill(0, 0, 0);
  if (dist(mouseX, mouseY, 362, height-75)<45) {
    cursor(HAND);
    if (mousePressed) {
      l=3;
    }
  } else if (dist(mouseX, mouseY, 585, height-75)<45) {
    cursor(HAND);
    if (mousePressed) {
      l=4;
      player.play();
    }
  } else {
  }
}
