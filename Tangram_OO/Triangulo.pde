class Triangulo extends Figura {
  float x1, y1, x2, y2, x3, y3;
  Triangulo(float tempX, float tempY,int ti,color tcol,float tx1, float ty1, float tx2, float ty2, float tx3, float ty3) {
    super(tempX, tempY,ti,tcol);
    x1=tx1;
    y1=ty1;
    x2=tx2;
    y2=ty2;
    x3=tx3;
    y3=ty3;
  }
  @Override void drawFigure() {
    triangle(x1, y1, x2, y2, x3, y3);
  }
}
